# tests/test.py
import unittest
from main import suma, resta


class TestFuncionesMatematicas(unittest.TestCase):
    def test_suma(self):
        self.assertEqual(suma(2, 3), 5)
        self.assertEqual(suma(-1, 1), 0)
        # Agrega más casos de prueba para la función de suma

    def test_resta(self):
        self.assertEqual(resta(5, 3), 2)
        self.assertEqual(resta(10, 5), 5)
        # Agrega más casos de prueba para la función de resta

    # Agrega más casos de prueba para otras funciones...


if __name__ == "__main__":
    unittest.main()
